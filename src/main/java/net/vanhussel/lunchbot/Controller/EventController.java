package net.vanhussel.lunchbot.Controller;

import net.vanhussel.lunchbot.Model.*;
import net.vanhussel.lunchbot.dto.MessageDTO;
import net.vanhussel.lunchbot.dto.MessageResponseDTO;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventController {

    private static final Logger log = LoggerFactory.getLogger(EventController.class);

    @Autowired
    private ModelMapper modelMapper;

    @Value("${token}")
    private String token;

    @Autowired
    private SlackClient client;

    @Autowired
    private LunchBot bot;

    @PostMapping("/api/event")
    MessageResponseDTO verify(@RequestBody MessageDTO messageDTO){

        InboundMessage inboundMessage = getInboundMessage(messageDTO);

        MessageResponse response = new MessageResponse();

        if(inboundMessage.isUrlVerification()){
            response.setChallenge(inboundMessage.getChallenge());
        }

        if(inboundMessage.isAppMention()) {

            client.sendMessage(bot.getResponseMessage(inboundMessage));
        }

        return modelMapper.map(response, MessageResponseDTO.class);
    }

    private InboundMessage getInboundMessage(@RequestBody MessageDTO messageDTO) {
        InboundMessage inboundMessage = modelMapper.map(messageDTO, InboundMessage.class);

        if(messageDTO.event!=null){
            inboundMessage.setEvent(modelMapper.map(messageDTO.getEventDTO(), Event.class));
        } else {
            inboundMessage.setEvent(modelMapper.map(messageDTO, Event.class));
        }
        return inboundMessage;
    }


}
