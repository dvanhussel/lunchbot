package net.vanhussel.lunchbot.config;

import net.vanhussel.lunchbot.Model.LunchBot;
import net.vanhussel.lunchbot.Model.OutboundMessage;
import net.vanhussel.lunchbot.Model.SlackClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Configuration
@EnableScheduling
public class ScheduledTasks {

    @Autowired
    LunchBot bot;

    @Autowired
    SlackClient slackClient;

    @Autowired
    BuildProperties buildProperties;

    @Value("${lunchMessageChannel}")
    private String lunchMessageChannel;

    @Value("${standupHour}")
    private int standupHour;

    @Value("${standupMinute}")
    private int standupMinute;


    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    @Scheduled(fixedRate = 60000)
    public void scheduleFixedRateTask() {

        log.debug("version: " + buildProperties.getVersion());
        log.debug("Running scheduled task, lunchtime: " + bot.getLunchTime());
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime tempDateTime = LocalDateTime.from(now);

        long seconds = tempDateTime.until(bot.getLunchTime(), ChronoUnit.SECONDS);
        log.debug("Seconds left: " + seconds);
        if (seconds < 0) {
            log.info("It's lunchtime!!!");
            sendLunchMessageToChannel();
            bot.resetLunchTime();
        }

        if(isStandupWarningTime()){
            sendStandupMessageToChannel();
        }
    }

    private boolean isStandupWarningTime() {
        log.info("Checking Daily standup time...");
        LocalDateTime now = LocalDateTime.now();
        return (now.getHour() == standupHour && now.getMinute() == standupMinute && isWorkDay());
    }

    private boolean isWorkDay() {
        LocalDateTime now = LocalDateTime.now();
        return now.getDayOfWeek().getValue() < 6;
    }

    private void sendStandupMessageToChannel() {
        OutboundMessage outboundMessage = new OutboundMessage();
        outboundMessage.setText("Het is... bijna tijd voor de *Daily Standup*!!!");
        outboundMessage.setChannel("#" + lunchMessageChannel);
        slackClient.sendMessage(outboundMessage);
    }

    private void sendLunchMessageToChannel() {
        OutboundMessage outboundMessage = new OutboundMessage();
        outboundMessage.setText("Het is... lunchtijd!!!!one!!");
        if (LocalDateTime.now().getDayOfWeek().getValue() == 5) {
            outboundMessage.setText("Tring, tring, tring.. op naar MR.BING!!!");
        }
        outboundMessage.setChannel("#" + lunchMessageChannel);
        slackClient.sendMessage(outboundMessage);

    }
}
