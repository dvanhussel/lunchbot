package net.vanhussel.lunchbot.Model;

public class InboundMessage {
    private String token;
    private String challenge;
    private String type;
    private Event event;


    public String getToken() {
        return token;
    }

    public InboundMessage setToken(String token) {
        this.token = token;
        return this;
    }

    public String getChallenge() {
        return challenge;
    }

    public InboundMessage setChallenge(String challenge) {
        this.challenge = challenge;
        return this;
    }

    public String getType() {
        return type;
    }

    public InboundMessage setType(String type) {
        this.type = type;
        return this;
    }

    public Event getEvent() {
        return event;
    }

    public InboundMessage setEvent(Event event) {
        this.event = event;
        return this;
    }

    public boolean isAppMention(){
        return (this.getType().equals("event_callback") && this.getEvent().getType().equals("app_mention"));
    }

    public boolean isUrlVerification(){
        return (this.getType().equals("url_verification"));
    }

}
