package net.vanhussel.lunchbot.Model;

public class OutboundMessage {
    private String text;
    private String user;
    private String channel;

    public String getText() {
        return text;
    }

    public OutboundMessage setText(String text) {
        this.text = text;
        return this;
    }

    public String getUser() {
        return user;
    }

    public OutboundMessage setUser(String user) {
        this.user = user;
        return this;
    }

    public String getChannel() {
        return channel;
    }

    public OutboundMessage setChannel(String channel) {
        this.channel = channel;
        return this;
    }

    public String getUserRef(){
        return "<@"+this.user+">";
    }
}
