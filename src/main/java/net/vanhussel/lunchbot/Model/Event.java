package net.vanhussel.lunchbot.Model;

public class Event {

    private String user;
    private String text;
    private String channel;
    private String event_ts;
    private String type;

    public String getUser() {
        return user;
    }

    public Event setUser(String user) {
        this.user = user;
        return this;
    }

    public String getText() {
        return text;
    }

    public Event setText(String text) {
        this.text = text;
        return this;
    }

    public String getChannel() {
        return channel;
    }

    public Event setChannel(String channel) {
        this.channel = channel;
        return this;
    }

    public String getEvent_ts() {
        return event_ts;
    }

    public Event setEvent_ts(String event_ts) {
        this.event_ts = event_ts;
        return this;
    }

    public String getType() {
        return type;
    }

    public Event setType(String type) {
        this.type = type;
        return this;
    }

    public String getUserRef(){
        return "<@"+this.user+">";
    }
}
