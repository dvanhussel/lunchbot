package net.vanhussel.lunchbot.Model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Component
public class LunchBot {

    private static final Logger log = LoggerFactory.getLogger(LunchBot.class);

    @Autowired
    BuildProperties buildProperties;

    private LocalDateTime lunchTime;
    private LocalDateTime lastLuchTime;

    public  LunchBot(){
       resetLunchTime();
       log.info("Lunchbot initialized.");
    }

    public LocalDateTime getLunchTime(){
        return this.lunchTime;
    }


    public void resetLunchTime(){

        this.lastLuchTime = this.lunchTime;

        Random r = new Random();
        Integer i =  r.nextInt(20);
        LocalDateTime lunchTime = LocalDate.now().atTime(11, 40+i);
        //Calculated lunchtime is in the past or on same day as last lunchtime, add extra day
        if(LocalDateTime.now().isAfter(lunchTime) || (this.lastLuchTime!=null && this.lastLuchTime.getDayOfWeek()==lunchTime.getDayOfWeek())){
            lunchTime = lunchTime.plusDays(1);
        }
        //Check if weekend
        if(lunchTime.getDayOfWeek().getValue()==6){
            lunchTime = lunchTime.plusDays(2);
        }
        if(lunchTime.getDayOfWeek().getValue()==7){
            lunchTime = lunchTime.plusDays(1);
        }
        this.lunchTime = lunchTime;
        log.info("New lunchtime set: "+this.lunchTime);
    }

    private String getLunchTimeAsString(){
        //Lunchtime is in the past, generate new lunchtime
        if(this.lunchTime.isBefore(LocalDateTime.now())){
            resetLunchTime();
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return this.lunchTime.format(formatter);
    }

    public OutboundMessage getResponseMessage(InboundMessage inboundMessage){

        String outboundText = getOutboundText(inboundMessage);

        OutboundMessage outboundMessage = new OutboundMessage();
        outboundMessage.setText(outboundText);
        outboundMessage.setChannel(inboundMessage.getEvent().getChannel());
        outboundMessage.setUser(inboundMessage.getEvent().getUser());

        return outboundMessage;
    }

    private String getOutboundText(InboundMessage inboundMessage){
        String inboundText = inboundMessage.getEvent().getText();
        String user = (inboundMessage.getEvent().getUser()!=null) ? inboundMessage.getEvent().getUserRef() : "";

        String outboundText = "*Sorry, dit begrijp ik niet.*\n\n*Probeer eens*: _Hoe laat gaan we lunchen?_";
        if(inboundText.toUpperCase().contains("TEST")){
            outboundText = "Breaky breaky!!";
        }
        if(inboundText.toUpperCase().contains("HOE LAAT")){
            if(this.lunchTime.getDayOfWeek() == LocalDateTime.now().getDayOfWeek()){
                outboundText = "Hoi *"+user+"*, lunch is vandaag om: *"+ getLunchTimeAsString()+"*";
            } else {
                outboundText = "Hoi *"+user+"*, lunch is morgen om: *"+ getLunchTimeAsString()+"*";
            }
        }
        if(inboundMessage.getEvent().getUser().equals("Ferry")){
            outboundText = "Jij mag niet lunchen *"+user+"*! Jij moet doorwerken";
        }
        return outboundText;
    }

}
