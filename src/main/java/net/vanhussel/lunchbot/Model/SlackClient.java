package net.vanhussel.lunchbot.Model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SlackClient {

    @Autowired
    BuildProperties buildProperties;

    @Value("${token}")
    private String token;

    private static final Logger log = LoggerFactory.getLogger(SlackClient.class);

    public void sendMessage(OutboundMessage message){

        RestTemplate restTemplate = new RestTemplate();
        String url = "https://slack.com/api/chat.postMessage";

        String slackMessage = "{\n" +
                "    \"text\": \""+message.getText()+"\n\n `SHH LunchBot version: "+buildProperties.getVersion()+"`\",\n" +
                "    \"channel\": \"" + message.getChannel() + "\",\n" +
                "    \"mrkdwn\": true\n" +
                "}";

        log.debug(slackMessage);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer "+token);

        HttpEntity<String> request = new HttpEntity<String>(slackMessage, headers);

        ResponseEntity<String> slackResonse = restTemplate.postForEntity(url, request, String.class);

        log.debug(slackResonse.getStatusCode().toString());
        log.debug(slackResonse.getBody());
    }
}
