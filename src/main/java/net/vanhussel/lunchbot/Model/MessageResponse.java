package net.vanhussel.lunchbot.Model;

public class MessageResponse {
    private String Challenge;

    public String getChallenge() {
        return Challenge;
    }

    public MessageResponse setChallenge(String challenge) {
        Challenge = challenge;
        return this;
    }
}
