package net.vanhussel.lunchbot.dto;

public class MessageResponseDTO {

    public String challenge;

    public String getChallenge() {
        return challenge;
    }

    public MessageResponseDTO setChallenge(String challenge) {
        this.challenge = challenge;
        return this;
    }
}
