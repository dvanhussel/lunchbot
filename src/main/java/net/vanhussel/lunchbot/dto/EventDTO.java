package net.vanhussel.lunchbot.dto;

public class EventDTO {
    public String user;
    public String text;
    public String channel;
    public String event_ts;
    public String type;

    public String getUser() {
        return user;
    }

    public EventDTO setUser(String user) {
        this.user = user;
        return this;
    }

    public String getText() {
        return text;
    }

    public EventDTO setText(String text) {
        this.text = text;
        return this;
    }

    public String getChannel() {
        return channel;
    }

    public EventDTO setChannel(String channel) {
        this.channel = channel;
        return this;
    }

    public String getEvent_ts() {
        return event_ts;
    }

    public EventDTO setEvent_ts(String event_ts) {
        this.event_ts = event_ts;
        return this;
    }

    public String getType() {
        return type;
    }

    public EventDTO setType(String type) {
        this.type = type;
        return this;
    }
}
