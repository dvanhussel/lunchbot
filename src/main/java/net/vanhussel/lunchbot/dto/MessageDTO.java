package net.vanhussel.lunchbot.dto;

public class MessageDTO {

    public String token;
    public String challenge;
    public String type;
    public EventDTO event;


    public String getToken() {
        return token;
    }

    public MessageDTO setToken(String token) {
        this.token = token;
        return this;
    }

    public String getChallenge() {
        return challenge;
    }

    public MessageDTO setChallenge(String challenge) {
        this.challenge = challenge;
        return this;
    }

    public String getType() {
        return type;
    }

    public MessageDTO setType(String type) {
        this.type = type;
        return this;
    }

    public EventDTO getEventDTO() {
        return event;
    }

    public MessageDTO setEventDTO(EventDTO eventDTO) {
        this.event = eventDTO;
        return this;
    }
}
